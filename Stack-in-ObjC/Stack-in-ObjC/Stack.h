//
//  Stack.h
//  Stack-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stack : NSObject {
@private
    int *m_pSatck;
    int m_iSatckCapacity;
    
    int m_iTop;
}

- (instancetype)initWithStackCapacity:(int)stackCapacity;
- (void)ClearStack;
- (BOOL)StackEmpty;
- (BOOL)StackFull;
- (int)StackLength;
- (BOOL)Push:(int)element;
- (BOOL)Pop:(int *)element;
- (void)StackTraverse;

@end
