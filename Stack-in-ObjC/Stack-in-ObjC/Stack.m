//
//  Stack.m
//  Stack-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import "Stack.h"

@implementation Stack

- (instancetype)initWithStackCapacity:(int)stackCapacity {
    self = [super init];
    if (self) {
        m_pSatck = (int *)malloc(sizeof(int)*stackCapacity);
        m_iSatckCapacity = stackCapacity;
        
        [self ClearStack];
    }
    return self;
}

- (void)ClearStack {
    m_iTop = 0;
}

- (BOOL)StackEmpty {
    return m_iTop == 0;
}

- (BOOL)StackFull {
    return m_iTop >= m_iSatckCapacity;
}

- (int)StackLength {
    return m_iTop;
}

- (BOOL)Push:(int)element {
    if ([self StackFull]) {
        return NO;
    }
    
    m_pSatck[m_iTop] = element;
    m_iTop += 1;
    return YES;
}

- (BOOL)Pop:(int *)element {
    if ([self StackEmpty]) {
        return NO;
    }
    
    m_iTop -= 1;
    *element = m_pSatck[m_iTop];
    return YES;
}

- (void)StackTraverse {
    for (int i = m_iTop - 1; i >= 0; i --) {
        NSLog(@"%d", m_pSatck[i]);
    }
    NSLog(@"\n");
}

@end
