//
//  main.m
//  Stack-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Stack.h"

void testStack();

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"testStack");
        testStack();
    }
    return 0;
}

void testStack() {
    Stack *s = [[Stack alloc] initWithStackCapacity:5];
    
    [s Push:2];
    [s Push:5];
    
    [s StackTraverse];
    
    int e = 0;
    [s Pop:&e];
    NSLog(@"e: %d\n", e);
    
    [s Push:7];
    [s Push:12];
    [s Push:19];
    [s Push:31];
    [s Push:51];
    
    [s StackTraverse];
    
    [s ClearStack];
    
    [s StackTraverse];
}
